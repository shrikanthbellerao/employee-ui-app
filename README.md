# employee-ui:
==============
This is the frontend application which allows to manage employee. 
Framework : Angular4 & Bootstrap
URL       : http:localhost:4200/
Component : EmployeeComponent


Architecture (Scope: only the frontend application):
----------------------------------------------------
I have implemented a decoupled approach where in I have 2 separate projects or applications:
1. Frontend application built using Angular 4 & Bootstrap
2. Backend application building using Java & Spring Boot/MongoDB

The benefit of decoupled approach is that the 2 applications are completely independent of each other in terms of:
- Technology or framework used to build
- Server on which the application runs etc.

1. During the initial load of the application, Routes module present in app.module.ts checks for the URL. 
    It currently handles '/employee' path only.
    All other paths are redirected to '/employee'. This path is mapped to EmployeeComponent.

2. Upon ngOnInit of EmployeeComponent, it invoked getAllEmployees function. Following steps occur:
    - This eventually invokes getAllEmployees function present in EmployeeService.
    - EmployeeService's getAllEmployees function makes a REST API (GET) call to the backend application.
    - The API returns a JSON array of all employee related info present in MongoDB.
    - This event is subscribed by the getAllEmployees function present in EmployeeComponent.
    - EmployeeComponent stores the response in employeeLst field which is used to render on screen = employee.component.html.
    - employee.component.html displays the list of employees in Bootstrap table on the right side of the screen.

3. Towards the left side is a section where user can enter/select info for adding a new Employee. The flow is given below:
    - User fills the form and clicks 'Add Employee' button.
    - This event submits the form and fnAddEmployee function present in EmployeeComponent is invoked.
    - Form data is later passed on to fnAddEmployee function present in EmployeeService.
    - This makes a REST API call (POST) to the backend application which persists the document in MongoDB
    - Backend application also returns the latest list of all the employees (along with the newly added employee)
    - fnAddEmployee function present in EmployeeComponent subscribes to the event where the response (JSON array) is received.
    - It then stores the response in employeeLst which is rendered on screen


Steps to compile & run the frontend application:
-----------------------------------------------
Open a command prompt and cd into the project folder
Run the ng-cli command: ng serve -o
This command creates the /dist folder and loads the application in a new browser tab
The frontend application assumes that the backend application is running @ http://localhost:8080/