import { Injectable } from '@angular/core';

@Injectable()
export class ScopeService {

  constructor() {
    // Nothing to do
  }

  // All the Global Variables should be listed here
  public userID: string         = '';
  public restApiServer: string  = 'http://localhost:8080/';
}
