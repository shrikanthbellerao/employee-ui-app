import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpEventType, HttpRequest } from '@angular/common/http';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { ScopeService} from './scope.service'

@Injectable()
export class UtilService {

  public header:any   = {
    headers           : new HttpHeaders({
      'Content-Type'  : 'application/json'
    })
  };

  constructor(
    private http: HttpClient,
    private router:Router,
    private scopeService:ScopeService) {
    // Nothing to do
  }

  // Function to check for empty string
  fnCES (inputStr) {

    if ((inputStr === undefined) || (inputStr === null) || (inputStr.toString().trim() === '') || (inputStr.toString().trim() === "")) {
      return true;
    } else {
      return false;
    }
  }

  // Function to check for empty object
  fnCEO (inputObj) {

    if ((inputObj === undefined) || (inputObj === null)) {
      return true;
    } else {
      return false;
    }
  }

  // Function to check for empty array
  fnCEA (inputArr) {

    if ((inputArr === undefined) || (inputArr === null) || (inputArr.length === 0)) {
      return true;
    } else {
      return false;
    }
  }
  
  // Format YYYY-MM-DD to MM/DD/YYYY
  fnFormatDate (dateStr) {
    
    var outputDate =  dateStr.substring(5, 7)   + '/' +
                      dateStr.substring(8, 10)  + '/' +
                      dateStr.substring(0, 4);
    return outputDate;
  }
}
