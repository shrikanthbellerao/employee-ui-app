import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { EmployeeComponent } from './components/employee/employee.component';

import { AuthGuard } from './services/auth.guard';
import { UtilService } from './services/util.service';
import { ScopeService } from './services/scope.service';

const appRoutes: Routes = [{
  path: 'employee',
  component: EmployeeComponent
},{
  path: '**',
  redirectTo: 'employee'
}];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes,
      { enableTracing: false } // <-- debugging purposes only
      ),
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [
    AuthGuard,
    UtilService,
    ScopeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }