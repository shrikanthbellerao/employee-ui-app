import { Injectable } from '@angular/core';
import { RequestOptions, Response, Http } from '@angular/http';
import "rxjs/add/operator/map";
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpEventType, HttpRequest } from '@angular/common/http';

import { UtilService } from '../../services/util.service';
import { ScopeService } from '../../services/scope.service';

@Injectable()
export class EmployeeService {

  public header:any   = {
    headers           : new HttpHeaders({
      'Content-Type'  : 'application/json'
    })
  };

  constructor(
    private http: HttpClient,
    private utilService: UtilService,
    private scopeService: ScopeService
  ) {
    // Nothing to do
  }

  // Function to make REST API call to fetch all employees
	fnFetchAllEmployees() {

    console.log("Inside fnFetchAllEmployees in EmployeeService");
    let responseObj = this.http.get(this.scopeService.restApiServer + 'employee/fetchAllEmployees' , this.header);
    console.log('responseObj ==>');console.log(responseObj);
    return responseObj;
  }

  // Function to make REST API call to add an employee
	fnAddEmployee(formData) {

    console.log("Inside fnAddEmployee in EmployeeService ==>");console.log(formData);
    let responseObj = this.http.post(this.scopeService.restApiServer + 'employee/addEmployee', formData, this.header);
    console.log('responseObj ==>');console.log(responseObj);
    return responseObj;
	}
}