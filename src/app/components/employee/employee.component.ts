import { Component, OnInit } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';

import { EmployeeService } from './employee.service';
import { ScopeService } from '../../services/scope.service';
import { UtilService } from '../../services/util.service';

import * as _ from 'lodash';
declare var $:any;

@Component({
  selector: 'employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  providers: [EmployeeService]
})
export class EmployeeComponent implements OnInit {

  constructor(
    private employeeService : EmployeeService,
  	private scopeService: ScopeService,
  	private utilService: UtilService
  ) {
    console.log("constructor of EmployeeComponent");
    // Nothing to do
  }

  ngOnInit() {
    console.log("ngOnInit of EmployeeComponent");
  	this.getAllEmployees();
  }

  public employeeLst:any        = [];
  public pleaseWaitFlg:boolean  = true;
  
  // Function to fetch all Employees
  public getAllEmployees() {

    console.log('Inside getAllEmployees');
  	this.pleaseWaitFlg = true;

    // Call service function to fetch all employees
  	this.employeeService.fnFetchAllEmployees().subscribe(res => {
  		console.log("Inside callback of fnFetchAllEmployees");console.log(res);
      this.pleaseWaitFlg  = false;
  		this.employeeLst    = res;

      for (var i in this.employeeLst) {
        this.employeeLst[i].dob = this.utilService.fnFormatDate(this.employeeLst[i].dob);
      }
    }, error => {
      this.pleaseWaitFlg  = false;
      console.log('Error occurred while making API call to fetch All Employees');
      console.log(error);
    })
  }
  
  // Function invoked when user clicks 'Add Employee' button
  public fnAddEmployee(formData) {
  
    console.log('Inside fnAddEmployee');console.log(formData);
  	this.pleaseWaitFlg = true;

    // Call service function to Add a new employee
  	this.employeeService.fnAddEmployee(formData).subscribe(res => {
  		console.log("Inside callback of fnAddEmployee");console.log(res);
      this.pleaseWaitFlg  = false;
  		this.employeeLst    = res;

      for (var i in this.employeeLst) {
        this.employeeLst[i].dob = this.utilService.fnFormatDate(this.employeeLst[i].dob);
      }
    }, error => {
      this.pleaseWaitFlg  = false;
      console.log('Error occurred while making API call to Add an Employee');
      console.log(error);
    })
  }
  
  // Function to sort the Employee list based on the column clicked
  public fnSortEmployeeLst (columnName) {
    
    console.log('Inside fnSortEmployeeLst: ' + columnName);
    this.employeeLst = _.orderBy(this.employeeLst, [columnName], ['asc']);
  }
}