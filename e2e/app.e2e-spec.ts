import { EmployeeUiAppPage } from './app.po';

describe('employee-ui-app App', () => {
  let page: EmployeeUiAppPage;

  beforeEach(() => {
    page = new EmployeeUiAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
